use std::convert::TryInto;

#[derive(Clone, Debug)]
struct Block {
	total: u8,

	// Digits available. Each bit represents a digit from 1-9.
	available_digits: u16,
	remaining_cells: u8,
}

impl Block {
	fn possible_digits(&self) -> u16 {
		let mut available = self.available_digits;
		let min_total: u8 = (1..=self.remaining_cells).sum();
		debug_assert!(min_total <= self.total,
				"Block total is too large for {} spaces. Got {} need {}",
				self.remaining_cells, self.total, min_total);
		let max_digit = self.remaining_cells + self.total - min_total;
		let max_digit = max_digit.min(9);
		available >>= 16 - 10 + 9 - max_digit;
		available <<= 16 - 10 + 9 - max_digit;

		let max_total: u8 = (1..=9).rev().take(self.remaining_cells.into()).sum();
		debug_assert!(max_total >= self.total,
				"self total is too small for {} spaces. Got {} need {}",
				self.remaining_cells, max_total, self.total);
		let min_digit = (10 - self.remaining_cells + self.total).saturating_sub(max_total).max(1);
		available <<= min_digit;
		available >>= min_digit;
		available
	}
}


#[derive(Clone, Debug)]
struct Board {
	width: u8,
	cells: Vec<u8>,
	blocks: Vec<Block>,
	block_map: Vec<(u8, u8)>,
	max: u8,
	solve_order: Vec<(u8, u8)>,
}

impl Board {
	fn new(width: u8, height: u8) -> Self {
		Board {
			width,
			cells: vec![0; width as usize * height as usize],
			blocks: vec![Block{total: 0, available_digits: 0, remaining_cells: 0}],
			block_map: vec![(255, 255); width as usize * height as usize],
			max: 0,
			solve_order: Vec::new(),
		}
	}

	pub fn width(&self) -> u8 {
		self.width
	}

	pub fn height(&self) -> u8 {
		(self.cells.len() / self.width as usize) as u8
	}

	fn cell_index(&self, x: u8, y: u8) -> usize {
		y as usize + x as usize * self.width as usize
	}

	fn cell_value(&self, x: u8, y: u8) -> Option<u8> {
		match self.cells[self.cell_index(x, y)] {
			0 => None,
			v => Some(v),
		}
	}

	fn set_cell_value(&mut self, x: u8, y: u8, v: u8) {
		debug_assert!(v != 0);

		let i = self.cell_index(x, y);
		self.cells[i] = v;
	}

	fn clear_cell(&mut self, x: u8, y: u8) {
		let i = self.cell_index(x, y);
		self.cells[i] = 0;
	}

	fn is_block(&self, x: u8, y: u8) -> bool {
		return self.block_map[self.cell_index(x, y)].0 == u8::max_value();
	}

	fn block_vertical(&self, x: u8, y: u8) -> Option<&Block> {
		if y == self.width() - 1 {
			None
		} else if self.block_map[self.cell_index(x, y)].1 != u8::max_value() {
			None
		} else if self.block_map[self.cell_index(x, y+1)].1 == u8::max_value() {
			None
		} else {
			Some(self.blocks(x, y+1).1)
		}
	}

	fn block_horizontal(&self, x: u8, y: u8) -> Option<&Block> {
		if x == self.height() - 1 {
			None
		} else if self.block_map[self.cell_index(x, y)].0 != u8::max_value() {
			None
		} else if self.block_map[self.cell_index(x+1, y)].0 == u8::max_value() {
			None
		} else {
			Some(self.blocks(x+1, y).0)
		}
	}

	fn block_indexes(&self, x: u8, y: u8) -> (usize, usize) {
		let (h, v) = self.block_map[self.cell_index(x, y)];
		debug_assert_ne!(h, u8::max_value(), "block_map[{}, {}] == {:?}", x, y, (h, v));
		debug_assert_ne!(v, u8::max_value(), "block_map[{}, {}] == {:?}", x, y, (h, v));
		(h as usize, v as usize)
	}

	fn blocks(&self, x: u8, y: u8) -> (&Block, &Block) {
		let (h, v) = self.block_indexes(x, y);
		(&self.blocks[h], &self.blocks[v])
	}

	fn blocks_mut(&mut self, x: u8, y: u8) -> (&mut Block, &mut Block) {
		let (h, v) = self.block_indexes(x, y);
		debug_assert!(h > v);
		let (vblocks, hblocks) = self.blocks.split_at_mut(h);
		(&mut hblocks[0], &mut vblocks[v])
	}

	/// Add a block with no influence. It is assumed there are additional blocks below this one.
	fn set_block(&mut self, x: u8, y: u8) {
		let cell_index = self.cell_index(x, y);
		self.block_map[cell_index] = (255, 255);
	}

	/// Add a block. Blocks must be set from left to right.
	fn set_block_horizontal(&mut self, x: u8, y: u8, total: u8) {
		let block_index = self.blocks.len();
		self.blocks.push(Block{
			total,
			available_digits: 0,
			remaining_cells: u8::max_value(),
		});

		let cell_index = self.cell_index(x, y);
		self.block_map[cell_index] = (u8::max_value(), u8::max_value());

		for x in x+1..self.width() {
			let cell_index = self.cell_index(x, y);
			self.block_map[cell_index].0 = block_index.try_into().expect("too many blocks");
		}
	}

	/// Add a block. Blocks must be set from top to bottom.
	fn set_block_vertical(&mut self, x: u8, y: u8, total: u8) {
		let block_index = self.blocks.len();
		self.blocks.push(Block{
			total,
			available_digits: 0,
			remaining_cells: u8::max_value(),
		});

		let cell_index = self.cell_index(x, y);
		self.block_map[cell_index] = (u8::max_value(), u8::max_value());

		for y in y+1..self.height() {
			let cell_index = self.cell_index(x, y);
			self.block_map[cell_index].1 = block_index.try_into().expect("too many blocks");
		}
	}

	/// Add two blocks equivelent to calling set_block_horizontal and set_block_vertical in sequence.
	fn set_block_hv(&mut self, x: u8, y: u8, horizontal: u8, vertical: u8) {
		self.set_block_horizontal(x, y, horizontal);
		self.set_block_vertical(x, y, vertical);
	}

	fn process(&mut self) {
		for block in &mut self.blocks {
			block.remaining_cells = 0;
		}

		for y in 0..self.height() {
			for x in 0..self.width() {
				if self.is_block(x, y) { continue }

				let (hi, vi) = self.block_indexes(x, y);
				self.blocks[hi].remaining_cells += 1;
				self.blocks[vi].remaining_cells += 1;
			}
		}

		for block in &mut self.blocks {
			// let mut mask = 0b0111_1111_1100_0000;
			let mask = u16::max_value();

			block.available_digits = mask;
		}

		self.calculate_solve_order();
	}

	fn solve_priority(&self, x: u8, y: u8) -> (u8, u8) {
		let (hblock, vblock) = self.blocks(x, y);
		let available = hblock.available_digits & vblock.available_digits;

		// We don't update the available as we don't know the most pessimistic value to remove. So we just assume everything is still available unless there are less than that many options in the remaining cells.
		let possiblities = (available.count_ones() as u8)
			.min(hblock.remaining_cells)
			.min(vblock.remaining_cells);

		let influence = hblock.remaining_cells + vblock.remaining_cells;

		(influence.wrapping_neg(), possiblities.wrapping_neg())
	}

	fn calculate_solve_order(&mut self) {
		// Assumes board is clear and processed.

		self.solve_order.clear();
		self.solve_order.reserve(self.cells.len() - self.blocks.len());

		let mut todo = (1..self.height())
			.map(|y| (1..self.width()).map(move |x| (x, y)))
			.flatten()
			.filter(|&(x, y)| !self.is_block(x, y))
			.map(|(x, y)| (self.solve_priority(x, y), x, y))
			.collect::<std::collections::BinaryHeap<_>>();

		while let Some((_, x, y)) = todo.pop() {
			if self.cell_value(x, y).is_some() { continue }

			self.set_cell_value(x, y, 1);
			let (hblock, vblock) = self.blocks_mut(x, y);
			hblock.remaining_cells -= 1;
			vblock.remaining_cells -= 1;

			fn l<'a>(this: &'a Board, line: impl Iterator<Item=(u8, u8)> + 'a)
				-> impl Iterator<Item=(u8, u8)> + 'a
			{
				line.take_while(move |&(x, y)| !this.is_block(x, y))
			}
			std::iter::empty()
				.chain(l(self, (1..x).map(|x| (x, y)).rev()))
				.chain(l(self, (x..self.width()).map(|x| (x, y))))
				.chain(l(self, (1..y).map(|y| (x, y)).rev()))
				.chain(l(self, (y..self.height()).map(|y| (x, y))))
				.for_each(|(x, y)| {
					todo.push((self.solve_priority(x, y), x, y));
				});

			self.solve_order.push((x, y));
		}

		for (x, y) in self.solve_order.clone() {
			self.clear_cell(x, y);
			let (hblock, vblock) = self.blocks_mut(x, y);
			hblock.remaining_cells += 1;
			vblock.remaining_cells += 1;
		}
	}

	fn solve(&mut self) -> bool {
		self.process();
		self.solve_from(0)
	}

	fn solve_from(&mut self, depth: u8) -> bool {
		let (x, y) = match self.solve_order.get(depth as usize) {
			Some(cell) => *cell,
			None => return true,
		};

		if depth > self.max {
			self.max = depth;
			eprintln!("solve_from({}, {}) -> {}/{} {}%",
				x, y,
				depth, self.solve_order.len(),
				100 * depth as usize / self.solve_order.len());
			eprintln!("{}", self);
		}

		let next = depth + 1;

		if self.is_block(x, y) { return self.solve_from(next) }

		let (horizontal_block_index, vertical_block_index) = self.block_indexes(x, y);

		let vertical_block = self.blocks[vertical_block_index].clone();
		let horizontal_block = self.blocks[horizontal_block_index].clone();

		let mut available = horizontal_block.possible_digits() & vertical_block.possible_digits();

		let min_remaining = horizontal_block.total.min(vertical_block.total);

		// eprintln!("h {:?}", horizontal_block);
		// eprintln!("v {:?}", vertical_block);
		// eprintln!("available {:016b}", available);

		while available != 0 {
			let v = available.leading_zeros() as u8;

			if v > min_remaining { break }

			debug_assert!(1 <= v && v <= 9, "Invalid digit {}", v);
			let mask = !(1 << (15 - v as u16));
			available &= mask;

			self.set_cell_value(x, y, v);
			self.blocks[vertical_block_index] = Block {
				total: vertical_block.total - v,
				available_digits: vertical_block.available_digits & mask,
				remaining_cells: vertical_block.remaining_cells - 1,
			};
			self.blocks[horizontal_block_index] = Block {
				total: horizontal_block.total - v,
				available_digits: horizontal_block.available_digits & mask,
				remaining_cells: horizontal_block.remaining_cells - 1,
			};

			if self.solve_from(next) { return true }
		}

		self.blocks[vertical_block_index] = vertical_block;
		self.blocks[horizontal_block_index] = horizontal_block;

		// cell.clear_value();
		return false
	}
}

impl std::fmt::Display for Board {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		for y in 0..self.height() {
			for x in 0..self.width() {
				if let Some(block) = self.block_horizontal(x, y) {
					write!(f, " \x1B[37;40m→{:2}\x1B[0m", block.total)?;
				} else if self.is_block(x, y) {
					write!(f, " \x1B[37;40m   \x1B[0m")?;
				} else if let Some(v) = self.cell_value(x, y) {
					write!(f, " {:^3}", v)?;
				} else {
					write!(f, "    ")?;
				}
			}
			writeln!(f)?;
			for x in 0..self.width() {
				if let Some(block) = self.block_vertical(x, y) {
					write!(f, " \x1B[37;40m↓{:2}\x1B[0m", block.total)?;
				} else if self.is_block(x, y) {
					write!(f, " \x1B[37;40m   \x1B[0m")?;
				} else {
					write!(f, "    ")?;
				}
			}
			writeln!(f)?;
		}

		Ok(())
	}
}

fn main() {
	let mut board = Board::new(3, 3);
	board.set_block_vertical(1, 0, 13);
	board.set_block_vertical(2, 0, 9);
	board.set_block_horizontal(0, 1, 12);
	board.set_block_horizontal(0, 2, 10);

	eprintln!("{}", board);
	board.solve();
	eprintln!("Done");
	eprintln!("{}", board);

	let mut board = Board::new(10, 10);
	board.set_block_vertical(1, 0, 26);
	board.set_block_vertical(2, 0, 18);
	board.set_block_vertical(3, 0, 8);
	board.set_block_vertical(4, 0, 26);
	board.set_block_vertical(6, 0, 17);
	board.set_block_vertical(7, 0, 35);
	board.set_block_vertical(8, 0, 26);
	board.set_block_vertical(9, 0, 13);
	board.set_block_horizontal(0, 1, 24);
	board.set_block_horizontal(5, 1, 30);
	board.set_block_horizontal(0, 2, 13);
	board.set_block_hv(5, 2, 21, 23);
	board.set_block_horizontal(0, 3, 34);
	board.set_block_hv(6, 3, 19, 37);
	board.set_block_horizontal(0, 4, 3);
	board.set_block_hv(3, 4, 22, 28);
	board.set_block(0, 5);
	board.set_block_vertical(1, 5, 13);
	board.set_block_hv(2, 5, 23, 22);
	board.set_block_vertical(8, 5, 17);
	board.set_block_vertical(9, 5, 30);
	board.set_block_horizontal(0, 6, 38);
	board.set_block_hv(7, 6, 11, 10);
	board.set_block_horizontal(0, 7, 15);
	board.set_block_hv(4, 7, 33, 3);
	board.set_block_horizontal(0, 8, 25);
	board.set_block_horizontal(5, 8, 13);
	board.set_block_horizontal(0, 9, 11);
	board.set_block_horizontal(5, 9, 17);

	eprintln!("{}", board);
	let start = std::time::Instant::now();

	let solved = board.solve();
	eprintln!("Solved: {}", solved);
	eprintln!("{}", board);

	eprintln!("Solved in {:?}", start.elapsed());
}
